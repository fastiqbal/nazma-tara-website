	    	<div class=""></div><!--Main body end-->
	    	<div class="footer footer-body">
	    		<div class="row footer-area">
	    			<div class="col-sm-2 col-lg-2 col-md-2"></div>
	    			<div class="col-sm-6 col-lg-6 col-md-6">
	    				<h5 class="copy-right">&copy; All right reserved by <a href="#">Nazma Tara</a></h5>
	    			</div>
	    			<div class="col-sm-4 col-lg-4 col-md-4">
	    				<div class="right-social">
    						<h4 class="followup">Follow us: <a href="https://www.facebook.com"><i class="fa fa-facebook-official" style="font-size:40px;color:#3B5998"></i></a>
    						<a href="https://www.linkedin.com"> <i class="fa fa-linkedin" style="font-size:40px;color:#0077B5"></i></a>
    						<a href="https://www.twitter.com"> <i class="fa fa-twitter" style="font-size:40px;color:#1DA1F2"></i></a>
    						</h4>
    					</div>
	    			</div>
	    		</div>
	    	</div><!--Main body end-->
	    </div>	<!--Main body end-->
	  </div><!-- /.container-fluid -->
	</nav>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap-3.3.5-dist/js/myscript.js"></script>

</body>
</html>