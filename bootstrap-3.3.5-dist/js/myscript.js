$(function() {
	
      var navlnks = document.querySelectorAll(".nav a");
        Array.prototype.map.call(navlnks, function(item) {

            item.addEventListener("click", function(e) {

                var navlnks = document.querySelectorAll(".nav a"); 

                Array.prototype.map.call(navlnks, function(item) {

                    if (item.parentNode.className == "active" || item.parentNode.className == "active open" ) {

                        item.parentNode.className = "";

                    } 

                }); 

                e.currentTarget.parentNode.className = "active";
            });
        });
	//highlight the current nav
	$("#home a:contains('Home')").parent().addClass('active');
	$("#about_me a:contains('About Me')").parent().addClass('active');
	$("#download_zone a:contains('Download Zone')").parent().addClass('active');
	$("#online_examination a:contains('Online Exam')").parent().addClass('active');
   	$("#write_me a:contains('Write Me')").parent().addClass('active');
	
	if($("#about_me a:contains('About Me')").parent().hasClass('active')){
	$(".dropdown a:contains('Our Programs')").parent().addClass('active');
	}
	
	if($("#joomla a:contains('Joomla Training')").parent().hasClass('active')){
	$(".dropdown a:contains('Our Programs')").parent().addClass('active');
	}

	//make menus drop automatically

	$('ul.nav li.dropdown').hover(function() {
		$('.dropdown-menu', this).fadeIn();
	}, function() {
		$('.dropdown-menu', this).fadeOut('fast');
	});//hover
	
}); //jQuery is loaded
            	