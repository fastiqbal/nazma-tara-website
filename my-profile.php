 <?php include("header.php");?>
	    <div class="main-body"><!--Main body start-->
	    	<div class="row body-content"><!--row start-->
	    		  <div class="col-sm-2 col-lg-2 col-md-4 left-block">		
					<img src="images/logo.jpg" class="img-responsive img-circle logo" alt="Responsive image">
					<!-- Indicates a successful or positive action -->
					<a href="assignment.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Assignment</button></a>	    	
					<a href="download-lecture-notes.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Lecture note</button></a>	    	
					<a href="results.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Results</button></a>	    	
					<a href="notice.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Notice</button></a>	    	
				  </div>
	  			  <div class="col-sm-10 col-lg-10 col-md-8 right-block">
	  			  	<div class="col-sm-10"></div>
	  			  	<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Professional Experience</p></button>
	  			  	<ul class="list-group">
  						<li class="list-group-item">
  							1.	National University, Gazipur<br>
  							<b>Assistant Professor</b><br>
							Dept. of Computer Science , October 20, 2015- present.<br>
							Teach students at M.S. level.


  						</li>
 						<li class="list-group-item">
 							2.	Stamford University Bangladesh, 744, Satmasjid Road, Dhanmondi, Dhaka-1209.,<br>
 							<b> Assistant Professor,</b><br>  
 							July 09, 2015 -  October 19, 2015.<br>
							Teach students at undergraduate level. Take class, supervise thesis, attach with several academic committees like course offer committee, exam committee, thesis distribution committee, etc.

 						</li>
  						<li class="list-group-item">
  							3.	Stamford University Bangladesh, 744, Satmasjid Road, Dhanmondi, Dhaka-1209., <br>
  							<b>Senior Lecturer, </b><br>
  							 March 09, 2013- July 08, 2015.<br>
  							 Teach students at undergraduate level. Take class, supervise thesis, attach with several academic committees
  						</li>
  						<li class="list-group-item">
  							4.	Stamford University Bangladesh, 744, Satmasjid Road, Dhanmondi, Dhaka-1209., <br>
  							<b>Lecturer, </b><br>
  							 May 14, 2011- March 08, 2013..<br>
  							 Teach students at undergraduate level. Take class, supervise thesis, attach with several academic committees
  						</li>
  						<li class="list-group-item">
  							5.	The People’s University of Bangladesh, 3/2 Asad avenue, Mohammadpur, Dhaka-1207, <br>
  							<b> Lecturer, </b><br>
  							 April 02, 2009- May 12, 2011<br>
  							 Teach students at undergraduate level. Take class, supervise thesis, attach with several academic committees
  						</li>
  						<!--<li class="list-group-item">Third item</li>-->
					</ul>
					<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Summery of Educatoin</p></button>
					<table class="table">
					   <thead>
						    <tr>
						        <th>Exam</th>
						        <th>Institute and Board/University</th>
						        <th>Session </th>
						        <th>Class/Division</th>
						        <th>Position</th>
					      	</tr>
					    </thead>
					    <tbody>
					      <tr>
						        <td>Ph.D.</td>
						        <td>Dept. of Computer Science and Engineering, University of Dhaka</td>
						        <td>Enrolling</td>
						        <td></td>
						        <td></td>
					      </tr>
					      <tr>
						        <td>M.Sc</td>
						        <td>Dept. of Computer Science and Engineering, University of Dhaka</td>
						        <td>2005-2006 Thesis group (Exam held in 2009)</td>
						        <td>1st Class, Marks : 65%.</td>
						        <td>44th</td>
					      </tr>
					      <tr>
						        <td>B.Sc.</td>
						        <td>Dept. of Computer Science and Engineering, University of Dhaka</td>
						        <td>2001-2005 (Exam held in 2007)</td>
						        <td>1st Class Marks: 63%.</td>
						        <td></td>
					      </tr>
					      <tr>
						        <td>H.S.C</td>
						        <td>Govt. M. M. City College, Khulna.Jessore Board</td>
						        <td>1998-1999 (Exam held in 2000)</td>
						        <td>1st Division Marks: 89.2%</td>
						        <td>2nd place in female merit list</td>
					      </tr>
					      <tr>
						        <td>S.S.C</td>
						        <td>Govt. Coronation Girls’ High School, Khulna. Jessore Board</td>
						        <td>1996-97(Exam held in 1998)</td>
						        <td>1st Division Marks: 89.4%</td>
						        <td>7th place in female merit list.</td>
					      </tr>

					    </tbody>
					  </table>
						<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Training, workshop and conference:</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  							<b>1.	IEEE conference on VLSI Design, Kolkata, India, January 3-8, 2016</b><br>
	  						</li>
	 						<li class="list-group-item">
	 							<b> 2.	IEEE International WIE Conference on Electrical and Computer Engineering (WIECON-ECE)</b><br>  
	 							19-20 December 2015, BUET, Dhaka, Bangladesh<br>
	 						</li>
	  						<li class="list-group-item">
	  							<b>3.	Workshop on Women Empowerment through ICT: Higher Studies, Research and Career</b><br>
	  							  2014 on June 7, 2014, (BUET), Dhaka<br>  						</li>
	  						<li class="list-group-item">
	  							<b>4.	Embedded System Design and Development, </b><br>
	  							 From23-29 January, 2013 Institute of Information and Communication Technology (IICT),(BUET), Dhaka.<br>  						</li>
	  						<li class="list-group-item">
	  							<b> 5.	Teacher’s Training, 2011 Center for Excellence (CFE),  </b><br>
	  							Stamford University Bangladesh.
	  						</li>
	  						<li class="list-group-item">
	  							<b> 6.	Understanding Development,</b><br> 
									From 08 August- 12 August 2009.<br>
								Dept. of Development  Studies, University of Dhaka.
	  						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
					 	<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Membership of Professional Group:</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  							<b>Member of IEEE. Membership No. 93873082</b>
	  						</li>
	 						<li class="list-group-item">
	 							<b> Member of Bangladesh Computer Society. Membership No. M-01862.</b>  
	 						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
						<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Software Skills</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  							<b>• Development tools : Visual Studio 6, Lex, Yacc, SPIM (Simulator for MIPS) </b>
	  						</li>

	 						<li class="list-group-item">
	 							<b> •	Web Design Software/tools:  Microsoft FrontPage, Dream weaver, wordpress, PHP,  Web framework-codeignetor.</b>  
	 						</li>
	  						<li class="list-group-item">
	 							<b>•	Other Software :  MS Office, Ms power point, Photoshop, Macromedia Flash 2006</b>  
	 						</li>
	 						<li class="list-group-item">
	 							<b>•	Platforms and Languages: DOS, Windows, Linux( Administration  - system + network )</b>  
	 						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
						<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Language Efficiency</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  							<b>Full proficiency with reading, writing, speaking & oral communication in Bengali and English.</b>
	  						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
						<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Extra Curricular Activities</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  								• Leadership ability in teamwork.<br>
									• Hobby is to read novels, watching movies.<br>
									• Comfortable in drawing picture.<br>
									• Interested in photography.<br>
									• Enthusiastic in challenging working field.<br>
									• Strong interpersonal communication skill.

	  						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
						<button type="button" class="btn btn-primary btn-lg btn-block "><p class="text-left">Personal Information</p></button>
						<ul class="list-group">
	  						<li class="list-group-item">
	  								<div class="row pesonal-info">
	  									<div class="col-md-2">Father’s Name</div><div class="col-md-10">Shah Md. Monsur Ali</div>
	  									<div class="col-md-2">Mother’s Name</div><div class="col-md-10">Rokeya Begum.</div>
	  									<div class="col-md-2">Spouse’s Name</div><div class="col-md-10">S.M. Bashir Ahmed</div>
										<div class="col-md-2">Date of birth </div><div class="col-md-10">December 1st, 1983</div>
										<div class="col-md-2">Religion</div><div class="col-md-10">Islam</div>
										<div class="col-md-2">Gender</div><div class="col-md-10">Female</div>
										<div class="col-md-2">Contact Address</div> <div class="col-md-10">20/1, Eskaton Garden Road, ‘Gardenia Apartment’ (flat # A2 ),  Dhaka-1207.</div>
										<div class="col-md-2">Permanent Address</div><div class="col-md-10">15 (old 9), Mirzapur Road, Khulna-9100.</div>
										<div class="col-md-2">Nationality</div><div class="col-md-10">Bangladeshi by birth</div>
										<div class="col-md-2">Contact No.</div><div class="col-md-10">+88-01710839236</div>

	  								</div>
	  						</li>
	  						<!--<li class="list-group-item">Third item</li>-->
						</ul>
	  			  </div>
	    	</div> <!--row end-->
 <?php include("footer.php");?>
