<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Home | Nazma Tara </title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body id="about">
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="index.php"><i class="fa fa-user fa-fw"></i> &nbsp; Nazma Tara<!--<img src="images/logo.jpg" alt="">--></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="index.php"><i class="fa fa-home fa-fw" aria-hidden="true"></i>&nbsp; Home<span class="sr-only">(current)</span></a></li>
	       <!-- <li><a href="#">Link</a></li>-->
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Me <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="my-profile.php">My Profile</a></li>
	            <li><a href="publication.php">Research work</a></li>
	            <li><a href="photo-galary.php">Photo galaray</a></li>
	           <!--
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">One more separated link</a></li>
	       		 -->
	          </ul>
	        </li>
	      </ul>
	      <form class="navbar-form navbar-right" role="search">
	        <div class="form-group">
	          <input type="text" class="form-control" placeholder="Search">
	        </div>
	            <button type="button" class="btn btn-default">
					<span class="glyphicon glyphicon-search"></span> Search
  				</button>
	      </form>
	      <ul class="nav navbar-nav navbar-left">
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Download Zone <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="download-lecture-notes.php">Lecture Notes</a></li>
	            <li><a href="assignment.php">Assignment</a></li>
	            <li><a href="results.php">Result</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	          </ul>
	        </li>
	        <li><a href="online-exam.php"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>&nbsp; Online Exam</a></li>
	        <li><a href="write-me.php"><i class="fa fa-envelope-o fa-fw"></i>&nbsp; Write me</a></li>
	      </ul>  
	    </div><!-- /.navbar-collapse -->
	    <div class="main-body"><!--Main body start-->
	    