 <?php include("header.php");?>
	    <div class="main-body"><!--Main body start-->
	    	<div class="row body-content"><!--row start-->
	    		<div class="col-sm-2 col-lg-2 col-md-4 left-block">		
					   <img src="images/logo.jpg" class="img-responsive img-circle logo" alt="Responsive image">
					   <!-- Indicates a successful or positive action -->
              <a href="assignment.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Assignment</button></a>       
              <a href="download-lecture-notes.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Lecture note</button></a>       
              <a href="results.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Results</button></a>       
              <a href="notice.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Notice</button></a>       
          </div>
	  			<div class="col-sm-10 col-lg-10 col-md-8 right-block">
	  				<h3 class="lecture-notes">Results</h3>
	  				<table class="table table-hover">
  						<thead>
  							<tr>
  								<th>Publishing date</th>
                  <th>Dept_Year_Semester</th>
  								<th>Batch</th>
  								<th>Course Title</th>
  								<th>Download</th>
  							</tr>
  						</thead>
  						<tbody>
  							<tr>
                  <td>13/12/16</td>
                  <td>MAS_CS_2015_1st</td>
                  <td>1st batch</td>
                  <td>Web Engineering</td>
                  <td>       
                    <a href="#" class="btn btn-info btn-lg">
                          <span class="glyphicon glyphicon-download"></span> Download
                      </a>
                    </td>
                </tr>
                <tr>
                  <td>13/12/16</td>
                  <td>MAS_CS_2015_1st</td>
                  <td>1st batch</td>
                  <td>Web Engineering</td>
                  <td>       
                    <a href="#" class="btn btn-info btn-lg">
                          <span class="glyphicon glyphicon-download"></span>Download
                      </a>
                    </td>
                </tr>
                <tr>
                  <td>13/12/16</td>
                  <td>MAS_CS_2015_1st</td>
                  <td>1st batch</td>
  								<td>Web Engineering</td>
  								<td>       
  								 	<a href="#" class="btn btn-info btn-lg">
          								<span class="glyphicon glyphicon-download"></span> Download
        						 	</a>
        						</td>
  							</tr>
  							
  						</tbody>
					</table>
				</div>
	    	</div> <!--row end-->
 <?php include("footer.php");?>