 <?php include("header.php");?>
	    <div class="main-body"><!--Main body start-->
	    	<div class="row body-content"><!--row start-->
	    		  <div class="col-sm-2 col-lg-2 col-md-4 left-block">		
					<img src="images/logo.jpg" class="img-responsive img-circle logo" alt="Responsive image">
					<!-- Indicates a successful or positive action -->
					<a href="assignment.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Assignment</button></a>	    	
					<a href="download-lecture-notes.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Lecture note</button></a>	    	
					<a href="results.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Results</button></a>	    	
					<a href="notice.php"><button type="button" class="btn btn-success btn-lg btn-menu-left">Notice</button></a>	    	
				  </div>
	  			  <div class="col-sm-10 col-lg-10 col-md-8 right-block">
	  			  	<div class="panel panel-default">
					    <div class="panel-heading">Publications:</div>
 					    <div class="panel-body">1.	<b>Nazma Tara</b> and Hafiz Md. Hasan Babu, <b><i>"Synthesis of Reversible PLA Using Products Sharing"</i></b>, Springer Journal of Computational Electronics. Journal of Computational Electronics,  June 2016, Volume 15, Issue 2, pp 420-428.<br>

 					    2.	Sworna, Zarrin Tasnim, Mubin Ul Haque, <b>Nazma Tara</b>, Hafiz Md Hasan Babu, and Ashis Kumar Biswas.<b><i> "Low-power and area efficient binary coded decimal adder design using a look up table-based field programmable gate array."</i></b> IET Circuits, Devices & Systems 10, no. 3 (2016): 163-172.<br>

 					    3.	<b>Tara, Nazma</b>, Hafiz Md Hasan Babu, and Nawshi Matin.<b><i> "Logic Synthesis in Reversible PLA.</i></b>" 2016 29th International Conference on VLSI Design and 2016 15th International Conference on Embedded Systems (VLSID). IEEE, 2016. <br>

 					    4.	Nafiul Islam Tony, <b>Nazma Tara </b>and Hafiz Md. Hasan Babu,<b><i>‘A New Approach for Design and Minimization of a Low Power  Reversible TANT Network’,</i></b> accepted in  IEEE International Symposium on Nanoelectronic and Information Systems (iNIS), 2015, Jakhy Indore, India, 21 Dec - 23 Dec 2015.<br>

 					    5.	<b>Nazma Tara,</b> Lafifa Jamal and Hafiz Md. Hasan Babu,<b><i>‘An Efficient Approach to Design Compact Reversible Programmable Logic Array’,</i></b> IEEE WIECON-ECE 2015,19-20 December, 2015,  BUET, Dhaka, Bangladesh.<br>

 					    6.	Ankur Sarker, Tanvir Ahmed, S.M. Mahbubur Rashid, <b>NazmaTara, </b>and Hafiz Md. Hasan Babu, <b><i>‘Realization of Reversible logic in DNA computing’,</i></b> 11th IEEE conference on Bioinformatics and Bioengineering,2011,pp.261-265.<br>

 					    7.	Salma Begum, <b>Nazma Tara,</b> Sharmin Sultana,<b><i> ‘Energy-Efficient Target Coverage in Wireless Sensor Networks Based on Modified Ant Colony Algorithm’,</i></b> International Journal of Ad hoc, Sensor & Ubiquitous Computing (IJASUC) , Vol. 1,No. 4, December 2010, pp 29-36.<br>
 					    8.	Sharmin Sultana, Salma Begum, <b>Nazma Tara </b>and Ahsan Raja Chowdhury,<b><i> ‘Enhanced-DSR: A New Approach to Improve Performance of DSR Algorithm’, </i></b>International Journal of Computer Science and Information Technology, Volume 2, Number 2, April 2010, pp.115-125.<br>
 					    9.	<b>Nazma Tara,</b> Ahsan Raja Chowdhury, <b><i>‘Design and Analysis of Error Detection Module for Reversible On-Line Testable Circuits’.,</i></b> Silver Jubilee Conference on Communication Technologies & VLSI Design (CommV’09),  VIT University, Vellore, India. Oct. 8-10, 2009. pp. 266-267.<br>

 					    10.	Sayed Ahsanul Kabir, <b>Nazma Tara, <i>‘Information Visualization with Distributed System(For distributed collaborative environment)’,</i></b> Journal of PUB, vol. 4, no. 1, July-2009, pp.-50-55.
 					    </div>
				    </div>
	  			  </div>
	    	</div> <!--row end-->
 <?php include("footer.php");?>
